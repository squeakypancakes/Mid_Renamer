#!/bin/bash

clear
echo "This script will rename Doom 2 midis for you. Numbers will be added to
map names to keep them in order"
echo ""

read -p "Map 01 " map01
read -p "Map 02 " map02
read -p "Map 03 " map03
read -p "Map 04 " map04
read -p "Map 05 " map05
read -p "Map 06 " map06
read -p "Map 07 " map07
read -p "Map 08 " map08
read -p "Map 09 " map09
read -p "Map 10 " map10
read -p "Map 11 " map11
read -p "Map 12 " map12
read -p "Map 13 " map13
read -p "Map 14 " map14
read -p "Map 15 " map15
read -p "Map 16 " map16
read -p "Map 17 " map17
read -p "Map 18 " map18
read -p "Map 19 " map19
read -p "Map 20 " map20
read -p "Map 21 " map21
read -p "Map 22 " map22
read -p "Map 23 " map23
read -p "Map 24 " map24
read -p "Map 25 " map25
read -p "Map 26 " map26
read -p "Map 27 " map27
read -p "Map 28 " map28
read -p "Map 29 " map29
read -p "Map 30 " map30
read -p "Map 31 " map31
read -p "Map 32 " map32
read -p "Intermission music " interMus
read -p "Text Screen Music " TSM
read -p "Title Music " TitleMus

# make sure files are all capital
for i in *.mid; do j="${i%.mid}"; mv "$i" "${j^^}.mid"; done

mv D_RUNNIN.mid "01 - ${map01%.*}.mid";
mv D_STALKS.mid "02 - ${map02%.*}.mid";
mv D_COUNTD.mid "03 - ${map03%.*}.mid";
mv D_BETWEE.mid "04 - ${map04%.*}.mid";
mv D_DOOM.mid "05 - ${map05%.*}.mid";
mv D_THE_DA.mid "06 - ${map06%.*}.mid";
mv D_SHAWN.mid "07 - ${map07%.*}.mid";
mv D_DDTBLU.mid "08 - ${map08%.*}.mid";
mv D_IN_CIT.mid "09 - ${map09%.*}.mid";
mv D_DEAD.mid "10 - ${map10%.*}.mid";
mv D_STLKS2.mid "11 - ${map11%.*}.mid";
mv D_THEDA2.mid "12 - ${map12%.*}.mid";
mv D_DOOM2.mid "13 - ${map13%.*}.mid";
mv D_DDTBL2.mid "14 - ${map14%.*}.mid";
mv D_RUNNI2.mid "15 - ${map15%.*}.mid";
mv D_DEAD2.mid "16 - ${map16%.*}.mid";
mv D_STLKS3.mid "17 - ${map17%.*}.mid";
mv D_ROMERO.mid "18 - ${map18%.*}.mid";
mv D_SHAWN2.mid "19 - ${map19%.*}.mid";
mv D_MESSAG.mid "20 - ${map20%.*}.mid";
mv D_COUNT2.mid "21 - ${map21%.*}.mid";
mv D_DDTBL3.mid "22 - ${map22%.*}.mid";
mv D_AMPIE.mid "23 - ${map23%.*}.mid";
mv D_THEDA3.mid "24 - ${map24%.*}.mid";
mv D_ADRIAN.mid "25 - ${map25%.*}.mid";
mv D_MESSG2.mid "26 - ${map26%.*}.mid";
mv D_ROMER2.mid "27 - ${map27%.*}.mid";
mv D_TENSE.mid "28 - ${map28%.*}.mid";
mv D_SHAWN3.mid "29 - ${map29%.*}.mid";
mv D_OPENIN.mid "30 - ${map30%.*}.mid";
mv D_EVIL.mid "31 - ${map31%.*}.mid";
mv D_ULTIMA.mid "32 - ${map32%.*}.mid";
mv D_DM2INT.mid "Inter - ${interMus%.*}.mid";
mv D_READ_M.mid "Text - ${TSM%.*}.mid";
mv D_DM2TTL.mid "00 - ${TitleMus%.*}.mid"
